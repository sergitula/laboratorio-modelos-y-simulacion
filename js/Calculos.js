
var datosUi = []; //VALOR GLOBAL DE ARRAY

var totalValoresDeN =[];
var seguirGenerando = true;
var numeroNMenorM = false;

function generarTabla(){
    let semilla = document.getElementById("semilla").value;
    let numeroN = document.getElementById("numeroN").value;
    let numFilas = document.getElementById("totalValores").value;
    let contenedorTabla = document.getElementById("contenedorTabla");
    let orden = 0;
    let mcuadrado, longitud, n ,U_i;
    datosUi = []; // RESETEAR ARRAY DE VALORES PARA QUE NO SE ACMULEN CON CADA ENTRADA...
    totalValoresDeN =[];
    seguirGenerando = true;

    contenedorTabla.innerHTML ="";
    let tabla = "<tbody>";
    
    for(let filas=1; filas<=numFilas; filas++){
        tabla+="<tr>";
        for(let columns=1; columns<=6; columns++){
            if(seguirGenerando == true){
                
                if(columns == 1){
                    orden ++;
                    tabla+="<td>" + orden + "</td>";
                }

                if(columns == 2 && filas==1){
                    tabla+="<td>" + semilla + "</td>";
                }else{
                    if(columns == 2){
                        
                        let copioN = n;
                        tabla+="<td>" + copioN + "</td>";
                    }
                }

                if(columns == 3 && filas ==1){
                    mcuadrado= Math.pow(semilla, 2);
                    tabla+="<td>" + mcuadrado + "</td>";
                }else{
                    if(columns==3){
                        mcuadrado= Math.pow(n, 2);                        
                        longitud = mcuadrado.toString().length;
                        // si la long es impar y n es par
                        if(longitud % 2 == 1 && numeroN % 2 == 0){
                            tabla+="<td>" + mcuadrado +"'0'"+ "</td>";
                        }else{
                            if(longitud % 2 == 0 && numeroN % 2 == 1){
                                tabla+="<td>" + mcuadrado +"'0'"+ "</td>";
                            }else{
                                tabla+="<td>" + mcuadrado + "</td>";
                            }
                            
                        }

                    }
                }

                if(columns ==4){
                    longitud = mcuadrado.toString().length;
                    tabla+="<td>" + longitud + "</td>";
                }

                if(columns == 5){
                    let inicio;
                    let fin;
                    // inicio = Math.round((longitud/2)-2);
                    
                    if(longitud >= parseInt(numeroN,10)){

                        if(parseInt(numeroN,10) == 2){
                            inicio = Math.round((longitud/2)-1);
                        }   
                        if(parseInt(numeroN,10) == 3){
                            if(longitud % 2 == 1){
                                inicio = Math.round((longitud/2)-2);
                            }else{
                                inicio = Math.round((longitud/2)-1);
                            }
                        }
                        if(parseInt(numeroN,10) == 4){
                            inicio = Math.round((longitud/2)-2);
                        }
                        if(parseInt(numeroN,10) == 5){
                            if(longitud % 2 == 1){
                                inicio = Math.round((longitud/2)-3);
                            }else{
                                inicio = Math.round((longitud/2)-2);
                            }
                        }
                        if(parseInt(numeroN,10) == 6){
                            inicio = Math.round((longitud/2)-3);
                        }
                        if(parseInt(numeroN,10) == 7){
                            if(longitud % 2 == 1){
                                inicio = Math.round((longitud/2)-4);
                            }else{
                                inicio = Math.round((longitud/2)-3);
                            }
                            
                        }
                        if(parseInt(numeroN,10) == 8){
                            inicio = Math.round((longitud/2)-4);
                        }
                        if(parseInt(numeroN,10) == 9){
                            if(longitud % 2 == 1){
                                inicio = Math.round((longitud/2)-5);
                            }else{
                                inicio = Math.round((longitud/2)-4);
                            }
                        }

                        // if(longitud % 2 == 1 && numeroN % 2 == 1){
                        //     inicio = Math.round((longitud/2)-3);
                        // }else{
                        //     inicio = Math.round((longitud/2)-2);
                        // }
                        fin = parseInt(numeroN, 10) + inicio;


                    
                        // if(seguirGenerando == true){
                        n = (mcuadrado.toString()).substring(inicio,fin);
                        totalValoresDeN.push(parseInt(n,10));
                        tabla+="<td>" + n + "</td>";
                        // }    
                    }else{
                        numeroNMenorM= true;
                        seguirGenerando= false;
                    }             

                }

                if(columns == 6){
                    U_i = parseFloat("0."+n);
                    tabla+="<td>" + U_i+ "</td>";

                    datosUi.push(U_i); //CARGAR ARRAY DE "Ui"
                    // agregarElementoAlArray(U_i);//CARGAR ARRAY DE "Ui"
                    //si este ultimo valor se encuentra dentro del array deja de iterar..
                    for(let i=0;i<totalValoresDeN.length -1 ;i++){
                        if(n == totalValoresDeN[i]){
                            seguirGenerando = false;
                        }
                    }
                }

            }else{
                if(columns==1 && filas ==  totalValoresDeN.length +1 ){
                    tabla+="<td class='text-danger'>"+"<strong> Semilla " + "'"+ n +"' se repite </strong>" +"</td>";
                    
                }else{
                    if(filas ==  totalValoresDeN.length +1 && (columns == 2 || columns == 3 || columns == 4 || columns == 5 || columns == 6)){
                        tabla+="<td class='text-danger'>" + "---" + "</td>";
                    }
                }
            }

        }
        tabla+="</tr>";
        
    }
    tabla += "</tbody>";
    contenedorTabla.innerHTML = tabla;

    generacionLimitada.innerHTML = "";
    if((numFilas != datosUi.length) && (datosUi.length != 0)){
        generacionLimitada.innerHTML= "Solamente se generaron '"+ datosUi.length+ "' valores válidos. ¿Realizar prueba únicamente con esos valores? " ;
    }
    contenedorTablaUi.innerHTML= "";
}


function generarTablaPruebas(){
    let OrdenUi = 0; 
    let sumatoria = 0;
    let promedio, estadistico;
    let estadisticoTabla = document.getElementById("estadisticoTabla").value;
    let colorTabla = document.getElementById("columnasColor");

    contenedorTablaUi.innerHTML= "";
    let tablaUi = "<thead>";
    for(let filas=0; filas<datosUi.length; filas++){
        tablaUi+="<tr>";
        for(let columns=1; columns<=4; columns++){
            if(columns == 1){
                OrdenUi ++;
                tablaUi+="<td>" + OrdenUi + "</td>";
            }
            if(columns == 2){ 
                tablaUi+="<td>" +datosUi[filas] + "</td>";
            }

            if(columns == 3 && filas == 0){
                for(let i=0; i<datosUi.length; i++){
                    sumatoria = sumatoria + datosUi[i];
                }
                promedio = sumatoria / datosUi.length;
                tablaUi+="<td>" +promedio.toFixed(4)+ "</td>";
            }
            if(columns == 4 && filas == 0){
                estadistico = Math.abs( (((promedio - (1/2)))* Math.sqrt(datosUi.length))/ Math.sqrt(1/12) );
                tablaUi+="<td>" +estadistico.toFixed(4)+ "</td>";
            }
        }
        tablaUi+="</tr>";
    }
    tablaUi += "</thead>";
    contenedorTablaUi.innerHTML = tablaUi;


    //BRINDAR EL RESULTADO INSERTANDO EL ESTADISTICO DE TABLA
    if((estadistico < estadisticoTabla)&& estadisticoTabla != "" ){
        document.getElementById("resultado").value = "La serie está uniformemente distribuida.";
        resultado.style.backgroundColor="#66ff33";
    }else{
        document.getElementById("resultado").value ="La serie 'NO' está uniformemente distribuida.";
        resultado.style.backgroundColor="#e4605e";
    }
}
